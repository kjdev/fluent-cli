fluent-cli
==========

```
Usage: fluent-cli [OPTIONS] <TAG>

  -h, --host=HOST      fluent host [DEFAULT: 127.0.0.1]
  -p, --port=PORT      fluent port [DEFAULT: 24224]
  -s, --socket=TYPE    fluent socket type [DEFAULT: tcp]
      --tcp            use fluent tcp port (-T|-s tcp)
      --udp            use fluent udp port (-U|-s udp)
  -f, --format=FORMAT  input format [DEFAULT: text]
      --text           input format plain text (-t|-f text)
      --json           input format json (-j|-f json)
  -A  --append         append message json data
  -w, --wait           wait standard input
  -c, --timeout=[msec] connection timeout [DEFAULT: none]
  -d, --dump           print sent message on errore
  -q, --quit           quit message
```
