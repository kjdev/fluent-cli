/*
 * fluent-cli
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <libgen.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include <jansson.h>

#define VERSION "0.5.3"

enum {
  SOCKET_TCP = 1,
  SOCKET_UDP,
  FORMAT_TEXT,
  FORMAT_JSON,
  LOGGER_ERROR,
  LOGGER_WARNING,
  LOGGER_DEBUG
};

#define DEFAULT_HOST "127.0.0.1"
#define DEFAULT_PORT 24224
#define DEFAULT_TRANSPORT SOCKET_TCP
#define DEFAULT_FORMAT FORMAT_TEXT

#define logger(level, ...)                                 \
  do {                                                     \
    switch (level) {                                       \
      case LOGGER_ERROR:                                   \
        fprintf(stderr, "\e[31m");                         \
        fprintf(stderr, "ERROR: "__VA_ARGS__);             \
        break;                                             \
      case LOGGER_WARNING:                                 \
        fprintf(stderr, "\e[33m");                         \
        fprintf(stderr, "WARNING: "__VA_ARGS__);           \
        break;                                             \
      case LOGGER_DEBUG:                                   \
        fprintf(stderr,                                    \
                "\e[35m%s\e[36m:\e[32m%d\e[36m:\e[30;1m ", \
                basename(__FILE__), __LINE__);             \
        fprintf(stderr, "DEBUG: "__VA_ARGS__);             \
        break;                                             \
      default:                                             \
        fprintf(stderr, __VA_ARGS__);                      \
        break;                                             \
    }                                                      \
    fprintf(stderr, "\e[m");                               \
    fflush(stderr);                                        \
  } while(0)

#define ERROR(...) logger(LOGGER_ERROR, __VA_ARGS__)
#define WARNING(...) if (verbose > 0) { logger(LOGGER_WARNING, __VA_ARGS__); }
#ifndef NDEBUG
#define DEBUG(...) if (verbose > 2) { logger(LOGGER_DEBUG, __VA_ARGS__); }
#else
#define DEBUG(...)
#endif

static int verbose = 0;

static void
usage(char *arg, char *message)
{
  char *command = basename(arg);

  printf("Usage: %s [OPTIONS] <TAG>\n\n", command);

  printf("  -h, --host=HOST      fluent host [DEFAULT: %s]\n", DEFAULT_HOST);
  printf("  -p, --port=PORT      fluent port [DEFAULT: %d]\n", DEFAULT_PORT);
  printf("  -s, --socket=TYPE    fluent socket type [DEFAULT: tcp]\n");
  printf("      --tcp            use fluent tcp port (-T|-s tcp)\n");
  printf("      --udp            use fluent udp port (-U|-s udp)\n");
  printf("  -f, --format=FORMAT  input format [DEFAULT: text]\n");
  printf("      --text           input format plain text (-t|-f text)\n");
  printf("      --json           input format json (-j|-f json)\n");
  printf("  -A  --append         append message json data\n");
  printf("  -w, --wait           wait standard input\n");
  printf("  -c, --timeout=[msec] connection timeout [DEFAULT: none]\n");
  printf("  -d, --dump           print sent message on error\n");
  printf("  -q, --quit           quit message\n");
  printf("  -V, --version        print version\n");

  if (message) {
    printf("\nINFO: %s\n", message);
  }
}

static void
append_message(json_t *json, char *msg)
{
  json_t *dat = NULL;
  json_error_t error;

  if (!json || !msg) {
    return;
  }

  dat = json_loads(msg, 0, &error);
  if (!dat) {
    DEBUG("Invalid load append data = %s\n", msg);
    return;
  } else if (!json_is_object(dat)) {
    DEBUG("Undefined load append json data = %s\n", msg);
  } else {
    const char *key;
    json_t *value;
    void *iter = json_object_iter(dat);
    while(iter) {
      key = json_object_iter_key(iter);
      value = json_object_iter_value(iter);

      json_object_set_new(json, key, json_copy(value));

      iter = json_object_iter_next(dat, iter);
    }
  }

  json_delete(dat);
}

#define send_message_dump()                           \
  do {                                                \
    if (dump && buf && len) {                         \
      ERROR("Sent Message = %.*s\n", (int)len, buf) ; \
    }                                                 \
  } while(0)

int
main (int argc, char **argv)
{
  int32_t opt;
  int32_t port = DEFAULT_PORT;
  int32_t transport = DEFAULT_TRANSPORT;
  int32_t format = DEFAULT_FORMAT;
  int32_t wait = false;
  uint32_t timeout = 0;
  int32_t dump = false;
  char *host = DEFAULT_HOST;
  char *tag = NULL;
  char *append = NULL;
  char buf[BUFSIZ] = {0};

  const struct option long_options[] = {
    { "host", 1, NULL, 'h' },
    { "port", 1, NULL, 'p' },
    { "socket", 1, NULL, 's' },
    { "tcp", 0, NULL, 'T' },
    { "udp", 0, NULL, 'U' },
    { "format", 1, NULL, 'f' },
    { "text", 0, NULL, 't' },
    { "json", 0, NULL, 'j' },
    { "append", 1, NULL, 'A' },
    { "wait", 0, NULL, 'w' },
    { "timeout", 1, NULL, 'c' },
    { "dump", 0, NULL, 'd' },
    { "quit", 0, NULL, 'q' },
    { "help", 0, NULL, 'H' },
    { "verbose", 0, NULL, 'v' },
    { "version", 0, NULL, 'V' },
    { NULL, 0, NULL, 0 }
  };

  while ((opt = getopt_long(argc, argv, "h:p:s:TUf:tjA:wc:dqHvV",
                            long_options, NULL)) != -1) {
    switch (opt) {
      case 'h':
        host = optarg;
        break;
      case 'p':
        port = atoi(optarg);
        if (port <= 0) {
          port = DEFAULT_PORT;
        }
        break;
      case 's':
        if (strcasecmp(optarg, "tcp") == 0) {
          transport = SOCKET_TCP;
        } else if (strcasecmp(optarg, "udp") == 0) {
          transport = SOCKET_UDP;
        }
        break;
      case 'T':
        transport = SOCKET_TCP;
        break;
      case 'U':
        transport = SOCKET_UDP;
        break;
      case 'f':
        if (strcasecmp(optarg, "json") == 0) {
          format = FORMAT_JSON;
        } else if (strcasecmp(optarg, "text") == 0) {
          format = FORMAT_TEXT;
        }
        break;
      case 't':
        format = FORMAT_TEXT;
        break;
      case 'j':
        format = FORMAT_JSON;
        break;
      case 'A':
        append = optarg;
        break;
      case 'w':
        wait = true;
        break;
      case 'c':
        timeout = atol(optarg);
        if (timeout <= 0) {
          timeout = 0;
        }
        break;
      case 'd':
        dump = true;
        break;
      case 'q':
        verbose = 0;
        break;
      case 'v':
        verbose += 1;
        break;
      case 'V':
        printf("%s %s\n", basename(argv[0]), VERSION);
        return 0;
      case 'H':
      default:
        usage(argv[0], NULL);
        return 1;
    }
  }

  if (argc <= optind) {
    usage(argv[0], "required tag.");
    return 1;
  }

  // Get tag
  tag = argv[optind];
  DEBUG("TAG = %s\n", tag);
  if (!tag || strlen(tag) == 0) {
    WARNING("empty tag\n");
    return 1;
  }

  if (!wait) {
    struct stat st;
    if (fstat(STDIN_FILENO, &st) != 0 || !S_ISFIFO(st.st_mode)) {
      WARNING("no such stdin.\n");
      return 1;
    }
  }

  while (fgets(buf, sizeof(buf), stdin) != NULL) {
    uint32_t len = strlen(buf);
    if (len > 0 && buf[len-1] == '\n') {
      buf[--len] = '\0';
    }

    DEBUG("Read stdin = %s\n", buf);

    int sockfd = -1;
    struct sockaddr_in addr;
    time_t now;
    char *message= NULL;
    json_t *in, *msg = NULL;

    if (format == FORMAT_JSON) {
      // Input format json
      json_error_t error;
      in = json_loadb(buf, len, 0, &error);
      if (!in) {
        ERROR("load json: %.*s\n", (int)len, buf);
        return 1;
      }
    } else {
      // Input format text
      in = json_object();
      if (!in) {
        ERROR("initialize json\n");
        return 1;
      }
      json_object_set_new(in, "message", json_stringn(buf, len));
    }

    // Connect
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = inet_addr(host);
    if (addr.sin_addr.s_addr == 0xffffffff) {
      struct hostent *hostn = gethostbyname(host);
      if (hostn == NULL) {
        ERROR("host address not found: %s\n", host);
        json_delete(in);
        return 1;
      }
      addr.sin_addr.s_addr = *(unsigned int *)hostn->h_addr_list[0];
    }

    DEBUG("Connect host = %s\n", host);
    DEBUG("Connect port = %d\n", port);

    // Initialize socket, message
    if (transport == SOCKET_UDP) {
      // Scoket UDP
      DEBUG("Connect type = UDP\n");

      sockfd = socket(AF_INET, SOCK_DGRAM, 0);
      if (sockfd < 0) {
        ERROR("socket open\n");
        json_delete(in);
        send_message_dump();
        return 1;
      }

      json_object_set_new(in, "tag", json_string(tag));

      append_message(in, append);

      msg = in;
    } else {
      // Socket TCP
      DEBUG("Connect type = TCP\n");

      sockfd = socket(AF_INET, SOCK_STREAM, 0);
      if (sockfd < 0) {
        ERROR("socket open\n");
        json_delete(in);
        send_message_dump();
        return 1;
      }

      if (timeout > 0) {
        // Connection with timeout

        // Non-Blocking socket
        if (fcntl(sockfd, F_SETFL, O_NONBLOCK) != 0) {
          ERROR("non blocking socket: %s:%d\n", host, port);
          close(sockfd);
          json_delete(in);
          send_message_dump();
          return 1;
        }

        // Timeout
        struct timeval tv;
        tv.tv_sec = timeout / 1000;
        tv.tv_usec = (timeout - (tv.tv_sec * 1000)) * 1000;

        int retval = connect(sockfd, (struct sockaddr *)&addr, sizeof(addr));
        DEBUG("connect -> %d\n", retval);
        if (retval < 0) {
          if (errno != EINPROGRESS) {
            ERROR("connection: %s:%d\n", host, port);
            close(sockfd);
            json_delete(in);
            send_message_dump();
            return 1;
          }

          fd_set fdset;
          FD_ZERO(&fdset);
          FD_SET(sockfd, &fdset);

          retval = select(sockfd+1, NULL, &fdset, NULL, &tv);
          DEBUG("select -> %d\n", retval);
          if (retval < 0) {
            ERROR("connection (select): %s:%d\n", host, port);
            close(sockfd);
            json_delete(in);
            send_message_dump();
            return 1;
          } else if (retval == 0) {
            ERROR("connection timeout(%ld): %s:%d\n", timeout, host, port);
            close(sockfd);
            json_delete(in);
            send_message_dump();
            return 1;
          }

          int so_error;
          socklen_t so_len = sizeof(so_error);
          getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &so_error, &so_len);
          DEBUG("so_error -> %d\n", so_error);
          if (so_error != 0) {
            ERROR("connection (sol): %s:%d\n", host, port);
            close(sockfd);
            json_delete(in);
            send_message_dump();
            return 1;
          }
        }
      } else {
        // Connection
        if (connect(sockfd, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
          ERROR("connect: %s:%d\n", host, port);
          close(sockfd);
          json_delete(in);
          send_message_dump();
          return 1;
        }
      }

      time(&now);

      msg = json_array();
      if (!msg) {
        ERROR("initialize json\n");
        json_delete(in);
        send_message_dump();
        return 1;
      }

      json_array_append_new(msg, json_string(tag));
      json_array_append_new(msg, json_integer(now));

      append_message(in, append);

      json_array_append_new(msg, in);
    }

    message = json_dumps(msg, JSON_COMPACT|JSON_ENCODE_ANY|JSON_ENSURE_ASCII);
    json_delete(msg);

    if (!message) {
      ERROR("undefined message\n");
      send_message_dump();
      return 1;
    }

    // Write socket
    DEBUG("Write message = %s\n", message);

    if (sendto(sockfd, message, strlen(message), 0,
               (struct sockaddr *)&addr, sizeof(addr)) == -1) {
      ERROR("write socket\n");
      send_message_dump();
    }

    close(sockfd);
    free(message);

    if (!wait) {
      break;
    }
  }

  return 0;
}
